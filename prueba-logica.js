var appVue = new Vue({
  el: '#app',
  data:{
    numeroPruebas: 0,
    pruebas: [],
    direcccionesFinales: [],
    errorNumeroPruebas: false,
    errorValidacionPruebas: false,
    bloquearBoton: false
  },
  watch:{
    numeroPruebas(val){
      this.errorValidacionPruebas = false;
      this.errorNumeroPruebas = (isNaN(val) || val < 1 || val > 5000);
    }
  },
  methods:{
    crearPruebas(){
      this.pruebas = [];
      this.direcccionesFinales = [];
      this.errorValidacionPruebas = false;
      this.bloquearBoton = false;

      if(!this.errorNumeroPruebas){
        for(let i = 1; i <= this.numeroPruebas;  i++){
          let id = (this.pruebas.length + 1);
          this.pruebas.push({id: id, x: null, y: null, valida: true});
        }

      }

    },

    existeEnArray(arrayDirecciones, cx, cy){
      let existe = arrayDirecciones.find((d) => {
        return (d.x === cx && d.y === cy);
      });
      return (typeof existe !== 'undefined');
    },

    recorrerArriba(movimientos, cx, sizeX, cy, sizeY){
      let contadorMovimientos = 0;
      let existe = false;

      for (let i = (cx-1); i >= 0; i--) {
        cx = i;
        if(!this.existeEnArray(movimientos, cx, cy)){
          movimientos.push({ x: cx, y: cy, d: "U" });
          contadorMovimientos++;
        }else{
          existe = true;
          break;
        }
      }

      if(contadorMovimientos > 0){
        movimientos = this.recorrerDerecha(movimientos, (cx + 1), sizeX, (cy + 1), sizeY); 
      }

      return movimientos;
    },

    recorrerIzquierda(movimientos, cx, sizeX, cy, sizeY){
      let contadorMovimientos = 0;
      let existe = false;

      for (let i = (cy-1); i >= 0; i--) {
        cy = i;
        if(!this.existeEnArray(movimientos, cx, cy)){
          movimientos.push({ x: cx, y: cy, d: "I" });
          contadorMovimientos++;
        }else{
          existe = true;
          break;
        }
      }

      cy = (contadorMovimientos > 0 && !existe) ? cy : (cy+1) ;
      movimientos = this.recorrerArriba(movimientos, cx, sizeX, cy, sizeY); 

      return movimientos;
    },

    recorrerAbajo(movimientos, cx, sizeX, cy, sizeY){
      let contadorMovimientos = 0;
      let existe = false;
      cx = cx + 1;

      for (let i = cx; i < sizeX; i++) {
        cx = i;
        if(!this.existeEnArray(movimientos, cx, cy)){
          movimientos.push({ x: cx, y: cy, d: "D" });
          contadorMovimientos++;
        }else{
          existe = true;
          break;
        }
      }

      cx = (contadorMovimientos > 0 && !existe) ? cx : (cx - 1);
      movimientos = this.recorrerIzquierda(movimientos, cx, sizeX, cy, sizeY); 

      return movimientos;
    },

    recorrerDerecha(movimientos, cx, sizeX, cy, sizeY){
      let contadorMovimientos = 0;
      let existe = false;

      for (let i = cy; i < sizeY; i++) {
        cy = i;
        if(!this.existeEnArray(movimientos, cx, cy)){
          movimientos.push({ x: cx, y: cy, d: "R" });
          contadorMovimientos++;
        }else{
          existe = true;
          break;
        }
      }

      cy = (contadorMovimientos > 0 && !existe) ? cy : (cy - 1);
      movimientos = this.recorrerAbajo(movimientos, cx, sizeX, cy, sizeY); 

      return movimientos;
    },

    recorrerEnEspiral(){
      for (let prueba of this.pruebas) {
        let movimientos = [];
        let movimientoFinal = [];
        let sizeX = prueba.y;
        let sizeY = prueba.x;
        let cx = 0;
        let cy = 0;
        movimientos = this.recorrerDerecha(movimientos, cx, sizeX, cy, sizeY);
        movimientoFinal = movimientos[movimientos.length - 1];
        this.direcccionesFinales.push(movimientoFinal);
      }
      this.bloquearBoton = false;
      console.log("se recorrio en espiral");
    },

    verificarPruebas(){
      this.pruebas = this.pruebas.map((p) =>{
        let xValida = (p.x !== null && p.x >= 1 && p.x <= Math.pow(10, 9));
        let yValida = (p.y !== null && p.y >= 1 && p.y <= Math.pow(10, 9));
        p.valida = (xValida && yValida);
        return p;
      });

      let error = this.pruebas.find((p)=>{
        return (!p.valida);
      });

      return Promise.resolve((typeof error === "undefined"));
    },

    validarPruebas(){
      this.direcccionesFinales = [];
      this.errorValidacionPruebas = false;

      this.verificarPruebas().then((response)=>{
        this.bloquearBoton = true;

        if(response){
          this.recorrerEnEspiral();
        }else{
          this.errorValidacionPruebas = true;
          this.bloquearBoton = false;
        }
      });
    }
  },
  mounted() {
//this.recorrerEnEspiral();
console.log('Alert Component Mounted')
}
})